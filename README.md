# Bitcoin Cash Node - BCH upgrade specifications

## Current state

This repository reflects the BCH upgrade specifications as implemented
(or planned to be implemented) by [Bitcoin Cash Node (BCHN)](https://bitcoincashnode.org/).

Current differences to the bitcoincash.org repository include:

- removal of inaccurate element: Infrastructure Funding Plan (IFP)

The specification in this repository is not considered "prescriptive" for
anyone besides the BCHN project.

It is considered descriptive and matches the upgrades planned by BCHN,
and authoritative only for our project until such time as a good place
can be agreed by the majority of BCH client projects for a common home
to such "delta" upgrade specifications.

The upgrade specifications are to be considered in the context of the
overall Bitcoin Cash specification that is being developed (see e.g.
<https://reference.cash>).

For implementation status in Bitcoin Cash Node, see
<https://docs.bitcoincashnode.org/doc/bch-upgrades/>.

## Plans

It is hoped that a shared, collaboratively managed repository for
Bitcoin Cash protocol specification, including changes proposed for
the regular upgrades, can be established soon.

The need for this repository might fall away once that is achieved -
the Bitcoin Cash Node could just refer to such a common repository in
future.

## License

The documents in this repository are released under various open licenses.
Please refer to the license information at the bottom of each document.
