---
layout: specification
title: 2020-NOV-15 Network Upgrade Specification
date: 2020-08-06
category: spec
activation: 1605441600
version: 0.2
---

# 2020-NOV-15 Network Upgrade Specification

Version 0.2, 2020-08-06

---


## Summary

When the median time past [1] of the most recent 11 blocks (MTP-11) is
greater than or equal to UNIX timestamp 1605441600, Bitcoin Cash will
execute an upgrade of the network consensus rules according to this
specification.

Starting from the next block these consensus rules changes will take effect:

* Bitcoin Cash's current difficulty adjustment algorithm will be replaced
  by a new algorithm named `aserti3-2d`, referred to as 'ASERT' for short.

## ASERT

The ASERT algorithm is described in [full specification: ASERT](2020-11-15-asert.md).

## Automatic Replay Protection

Both before and after this upgrade, Bitcoin Cash full nodes and wallets
MUST enforce the following rule:

 * `forkid` [2] to be equal to 0.

## References

[1] Median Time Past is described in [bitcoin.it wiki](https://en.bitcoin.it/wiki/Block_timestamp).
It is guaranteed by consensus rules to be monotonically increasing.

[2] The `forkId` is defined as per the [replay protected sighash](replay-protected-sighash.md) specification.


---

## License of this document

<small>The MIT License (MIT)</small>

<small>Copyright (c) 2020 The Bitcoin developers
<br>Copyright (c) 2017-2020 bitcoincash.org</small>

<small>Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:</small>

<small>The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.</small>

<small>THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.</small>
